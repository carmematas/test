#programa que calcula una àrea
print "Anem a calcula l'àrea d'un cercle"
print "Dóna el valor del radi del cercle en centímetres"
radi = input()
pi = 3.141596
area = pi*radi**2
print "L'àrea d'aquest cercle val \t %0.4f cm2" % (area)